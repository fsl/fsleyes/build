#!/usr/bin/env python
#
# build.py - Standalone FSLeyes builds
#
# Author: Paul McCarthy <pauldmccarthy@gmail.com>
#
"""Standalone FSLeyes builds.

This script builds standalone versions of FSLeyes for macOS or Linux, using
py2app or pyinstaller respectively.


You must install all FSLeyes and all of its dependencies prior to calling
``setup.py``.


*Important* Scipy must be compiled from source - do not install a pre-compiled
version!


For standalone Linux builds, you must install pyinstaller prior to calling
``setup..py``. Version 3.2.1 is known to work.


For standalone macOS builds, you must install py2app 0.14 and macholib 1.10
prior to calling ``setup.py``, and patch and recompile it as described below.


============
py2app notes
============


I am currently using py2app 0.14 for macOS builds. There are some issues with
this version of py2app which we need to work around:


https://bitbucket.org/ronaldoussoren/py2app/issues/140/
https://bitbucket.org/ronaldoussoren/py2app/issues/229/

Furthermore, i am using macholib 1.10, as more recent versions are incompatible
with this py2app 0.14.

Before calling this script, you need to patch the py2app installation.  The
patch found in ``resources/py2app.patch`` must be applied to the py2app
source::

    cd path/to/py2app/
    patch -p2 < path/to/fsleyes-build/resources/py2app.patch

And then the py2app bootstrap application needs to be recompiled::

    cd apptemplate
    python setup.py
"""


from __future__ import print_function

import               os
import               sys
import               glob
import               shutil
import               pkgutil
import               fnmatch
import               platform
import               logging
import               importlib
import               py_compile
import subprocess as sp
import itertools  as it
import os.path    as op

from collections import defaultdict
from io          import open

from setuptools import setup
from setuptools import Command
from setuptools import find_packages


# Directory that contains this script
basedir = op.abspath(op.dirname(__file__))


# Expected to be "darwin" or "linux"
platform = platform.system().lower()

# (major, minor) python version
pyver = sys.version_info[:2]


# if linux, we add some extra information
# if centos6/7 or ubuntu 1404/1604
if platform == 'linux':

    if op.exists(op.join(op.sep, 'etc', 'redhat-release')):
        with open(op.join(op.sep, 'etc', 'redhat-release'), 'rt') as f:
            info = f.read().lower()
            if 'centos' in info:
                if   ' 6.' in info: platform = 'centos6'
                elif ' 7.' in info: platform = 'centos7'
                elif ' 8.' in info: platform = 'centos8'

    elif op.exists(op.join(op.sep, 'etc', 'lsb-release')):
        with open(op.join(op.sep, 'etc', 'lsb-release'), 'rt') as f:
            info = f.read().lower()
            if 'ubuntu' in info:
                if   '14.04' in info: platform = 'ubuntu1404'
                elif '16.04' in info: platform = 'ubuntu1604'
                elif '18.04' in info: platform = 'ubuntu1804'
                elif '20.04' in info: platform = 'ubuntu2004'


if platform not in ('darwin',
                    'centos6',
                    'centos7',
                    'centos8',
                    'ubuntu1404',
                    'ubuntu1604',
                    'ubuntu1804',
                    'ubuntu2004'):
    raise RuntimeError('Unsupported platform')


try:
    from py2app.build_app import py2app as orig_py2app

# A dummy orig_py2app class, so my
# py2app class definition won't
# break if py2app is not present
except Exception:
    class orig_py2app(Command):
        user_options = []

        def initialize_options(self):
            pass

        def finalize_options(self):
            pass

        def run(self):
            pass


class build_standalone(Command):
    description = 'Build a standalone FSLeyes application ' \
                  'using py2app or pyinstaller.'
    user_options = [
        ('skip-patch-code', 'c', 'Skip code patch step'),
        ('skip-build',      'b', 'Skip build'),
        ('skip-archive',    'a', 'Skip archive creation'),
        ('enable-logging',  'l', 'Enable logging')
    ]

    boolean_options = [
        'skip-patch-code',
        'skip-build',
        'skip-archive',
        'enable-logging'
    ]

    def initialize_options(self):
        self.skip_patch_code = False
        self.skip_build      = False
        self.skip_archive    = False
        self.enable_logging  = False

    def finalize_options(self):
        pass

    def run(self):

        # Some combination of typing, jinja2, and possibly sphinx
        # causes errors to be raised when jinja2 is imported during
        # the py2app/pyinstaller build processes. Importing here
        # seems to work fine. I suspect that the problem lies with
        # the python typing module.
        import jinja2.utils    # noqa
        import jinja2.runtime  # noqa

        import fsleyes

        # Patch code (disable GL debug, remove logging, etc)
        if not self.skip_patch_code:
            self.patch_code(self.enable_logging)

        # run py2app or pyinstaller
        if not self.skip_build:
            if platform == 'darwin': self.run_command('py2app')
            else:                    self.run_command('pyinstaller')

        distdir = op.join(basedir, 'dist')

        # Get a ref to the distribution directory
        if platform == 'darwin': archivedir = 'FSLeyes.app'
        else:                    archivedir = 'FSLeyes'

        # install dcm2niix
        install_dcm2niix(op.join(distdir, archivedir, 'dcm2niix'))

        # create archive file
        if not self.skip_archive:
            archivefile = op.join(distdir,
                                  'FSLeyes-{}'.format(fsleyes.__version__))

            print('Creating {}.tar.gz...'.format(archivefile))
            shutil.make_archive(archivefile,
                                'gztar',
                                root_dir=distdir,
                                base_dir=archivedir)


    def patch_code(self, enableLogging):

        propsdir   = op.join(package_path('fsleyes_props'),
                             'fsleyes_props')
        widgetsdir = op.join(package_path('fsleyes_widgets'),
                             'fsleyes_widgets')
        fslpydir   = op.join(package_path('fsl'),
                             'fsl')
        fsleyesdir = op.join(package_path('fsleyes'),
                             'fsleyes')

        def patch_file(filename, linepatch):

            old = filename
            new = '{}.patch'.format(filename)

            with open(old, 'rt') as inf, \
                 open(new, 'wt') as outf:

                for line in inf:
                    outf.write(linepatch(line))

            os.rename(new, old)

        def patch_gl():

            def linepatch(line):
                if line.startswith('OpenGL.ERROR_CHECKING'):
                    line = 'OpenGL.ERROR_CHECKING = False\n'
                elif line.startswith('OpenGL.ERROR_LOGGING'):
                    line = 'OpenGL.ERROR_LOGGING = False\n'
                return line

            filename = op.join(fsleyesdir, 'gl', '__init__.py')

            print('Setting up OpenGL initialisation: {}'.format(filename))

            patch_file(filename, linepatch)

        def patch_version():

            import fsleyes

            version = fsleyes.__version__
            if 'FSLEYES_BUILD' in os.environ:
                version = '{}+build{}'.format(
                    version, os.environ['FSLEYES_BUILD'])

            print('Patching FSLeyes version:', version)

            # TODO You should patch fsleyes/version.py and
            #      add the local version identifier to
            #      the actual __version__ attribute. But
            #      doing so will break the update checking
            #      mechanism for all versions of FSLeyes
            #      that are using fslpy < 1.10.3 (which is
            #      the first version to support local
            #      version identifiers).
            #
            #      Perhaps change this with the next FSL
            #      release?

            # def linepatch(line):
            #     if line.startswith('__version__ ='):
            #         return '__version__ = \'{}\'\n'.format(version)
            #     else:
            #         return line
            # patch_file(op.join(fsleyesdir, 'version.py'), linepatch)

            # Instead we patch the about dialog, and the
            # output of the CLI --version option
            def linepatch(line):
                if 'version.__version__' in line:
                    return line.replace('version.__version__',
                                        '\'{}\''.format(version))
                return line

            patch_file(op.join(fsleyesdir, 'about.py'),     linepatch)
            patch_file(op.join(fsleyesdir, 'parseargs.py'), linepatch)


        def remove_logging():

            logstripdir = op.join(basedir, 'resources')
            sys.path.append(logstripdir)
            import logstrip

            logging.getLogger('logstrip').setLevel(logging.CRITICAL)

            propsfiles   = list_all_files(propsdir)
            widgetsfiles = list_all_files(widgetsdir)
            fslpyfiles   = list_all_files(fslpydir)
            fsleyesfiles = list_all_files(fsleyesdir)

            print('Removing logging: {}'.format(propsdir))
            print('Removing logging: {}'.format(widgetsdir))
            print('Removing logging: {}'.format(fslpydir))
            print('Removing logging: {}'.format(fsleyesdir))

            for filename in it.chain(
                    propsfiles, widgetsfiles, fslpyfiles, fsleyesfiles):
                if not filename.endswith('.py'):
                    continue
                logging.getLogger().setLevel(logging.WARNING)
                logstrip.main(['-f', '-M', 'INFO', filename])

        def enable_logging():

            def linepatch(line):
                if line.startswith('disableLogging'):
                    line = 'disableLogging = False\n'
                return line

            filename = op.join(fsleyesdir, '__init__.py')

            print('Enabling logging: {}'.format(filename))

            patch_file(filename, linepatch)

        patch_gl()
        patch_version()
        if self.enable_logging: enable_logging()
        else:                   remove_logging()


class py2app(orig_py2app):
    description = 'Builds a standalone FSLeyes OSX application using '\
                  'py2app. Not intended to be called directly.'

    def finalize_options(self):

        resourcedir = op.join(basedir, 'resources')
        entrypt     = op.join(basedir, 'entrypt.py')
        iconfile    = op.join(resourcedir, 'app_icon', 'fsleyes.icns')
        dociconfile = op.join(resourcedir, 'app_icon', 'fsleyes_doc.icns')
        plist       = op.join('resources', 'Info.plist')
        assets      = build_asset_list(False)

        self.verbose             = False
        self.quiet               = True
        self.argv_emulation      = True
        self.no_chdir            = True
        self.optimize            = True
        self.app                 = [entrypt]
        self.iconfile            = iconfile
        self.dociconfile         = dociconfile
        self.plist               = plist
        self.resources           = assets
        self.packages            = ['OpenGL_accelerate',
                                    'certifi',
                                    'fsl',
                                    'wxnat',
                                    'nibabel',
                                    'trimesh',
                                    'xnat',
                                    'jedi',
                                    'pygments',
                                    'IPython',
                                    'ipykernel',
                                    'ipykernel_launcher',
                                    'jinja2',
                                    'notebook',
                                    'nbformat',
                                    'jsonschema',
                                    'pkg_resources']
        self.matplotlib_backends = ['wxagg', 'ps', 'pdf', 'svg']
        self.excludes            = ['Cython']

        orig_py2app.finalize_options(self)


    def run(self):

        import fsleyes

        orig_py2app.run(self)

        version     = fsleyes.__version__
        copyright   = 'todo'

        distdir     = op.join(basedir, 'dist')
        contentsdir = op.join(distdir, 'FSLeyes.app', 'Contents')
        fwdir       = op.join(contentsdir, 'Frameworks')
        plist       = op.join(contentsdir, 'Info.plist')
        resourcedir = op.join(contentsdir, 'Resources')

        # copy the application document iconset
        shutil.copy(self.dociconfile, resourcedir)

        # Patch Info.plist
        commands = [
            ['delete', 'PythonInfoDict'],
            ['write',  'CFBundleShortVersionString', version],
            ['write',  'CFBundleVersion',            version],
            ['write',  'NSHumanReadableCopyright',   copyright],
        ]

        for c in commands:
            sp.call(['defaults'] + [c[0]] + [plist] + c[1:])

        # The defaults command screws with Info.plist
        # so make sure it has rw-r--r-- permissions
        os.chmod(plist, 0o644)


class pyinstaller(Command):
    description  = 'Builds a standalone FSLeyes Linux ' \
                   'application using pyinstaller. Not intended to be '\
                   'called directly.'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):

        builddir = op.join(basedir, 'build')
        distdir  = op.join(basedir, 'dist')
        assetdir = op.join(distdir, 'FSLeyes', 'share', 'FSLeyes')
        iconfile = op.join(assetdir, 'icons', 'app_icon', 'fsleyes.ico')
        assets   = build_asset_list(False)
        entrypt  = op.join(basedir, 'entrypt.py')

        def gen_packages(pkg):
            pkgs = find_packages(package_path(pkg, in_=True))
            return [pkg + '.' + p for p in pkgs]

        hidden = ([
            'scipy.special._ufuncs_cxx',
            'scipy.linalg.cython_blas',
            'scipy.linalg.cython_lapack',
            'scipy.integrate',
            'scipy._lib.messagestream',
            'OpenGL_accelerate',
            'OpenGL.platform.osmesa',
            'OpenGL.GLUT',
            'wx.__version__',
            'ipykernel_launcher',
            'ipykernel.datapub',
            'IPython.extensions.storemagic',
            'notebook.tree.handlers',
            'notebook.view.handlers',
            'notebook.notebook.handlers',
            'notebook.nbconvert.handlers',
            'notebook.bundler.handlers',
            'notebook.kernelspecs.handlers',
            'notebook.edit.handlers',
            'notebook.services.api.handlers',
            'notebook.services.config.handlers',
            'notebook.services.kernels.handlers',
            'notebook.services.contents.handlers',
            'notebook.services.sessions.handlers',
            'notebook.services.nbconvert.handlers',
            'notebook.services.kernelspecs.handlers',
            'notebook.services.security.handlers',
            'notebook.services.shutdown',
            'pkg_resources.py2_warn',
            'jupyter'] +
                  gen_packages('IPython') +  # noqa
                  gen_packages('ipykernel') +
                  gen_packages('ipython_genutils') +
                  gen_packages('jupyter_client') +
                  gen_packages('jupyter_core') +
                  gen_packages('nbconvert') +
                  gen_packages('nbformat') +
                  gen_packages('notebook'))

        if pyver < (3, 6):
            excludes = [
                'jinja2.asyncfilters',
                'jinja2.asyncsupport',
            ]
        else:
            excludes = []

        # Extra .so files to include in the bundle
        extrabins = ['glut',
                     'OSMesa',
                     'SDL-1.2',
                     # 'SDL2-2.0',
                     'notify',
                     'spatialindex',
                     'spatialindex_c']

        extrabins = [find_library(b)  for b in extrabins]
        extrabins = ['{}:.'.format(b) for b in extrabins]

        extrafiles  = self.include_package('nibabel')
        extrafiles += self.include_package('xnat')
        extrafiles += self.include_package('jedi',     'all')
        extrafiles += self.include_package('notebook', 'all')
        extrafiles += self.include_package('nbformat', 'all')
        extrafiles += self.include_package('trimesh',  'all')
        extrafiles += self.include_package('fsl',      'all')

        cmd = [
            'pyinstaller',
            '--log-level=WARN',
            '--name=FSLeyes',
            '--icon={}'.format(iconfile),
            '--windowed',
            '--workpath={}'.format(builddir),
            '--distpath={}'.format(distdir)
        ]

        for h in hidden:     cmd += ['--hidden-import',  h]
        for e in excludes:   cmd += ['--exclude-module', e]
        for e in extrabins:  cmd += ['--add-binary',     e]
        for e in extrafiles: cmd += ['--add-data',       e]

        cmd += [entrypt]

        sp.call(cmd)

        # Move the spec file into the build directory
        shutil.move(op.join(basedir, 'FSLeyes.spec'), builddir)

        # Make the executable lowercase
        try:
            os.rename(op.join(distdir, 'FSLeyes', 'FSLeyes'),
                      op.join(distdir, 'FSLeyes', 'fsleyes'))

        # Case insensitive file system?
        except Exception as e:
            print('Could not rename FSLeyes executable (ignoring):', e)

        # Something is wrong with the way
        # that PyOpenGL tries to find
        # the libglut library. If we make
        # a symlink, called 'glut', to the
        # .so file, things work fine.
        libglut = glob.glob(op.join(distdir, 'FSLeyes', 'libglut*'))
        if len(libglut) != 1:
            raise RuntimeError('Cannot identify libglut')

        os.symlink(op.basename(libglut[0]),
                   op.join(distdir, 'FSLeyes', 'glut'))

        # Similarly, something is wrong with
        # the way that rtree tries to access
        # libspatialindex...
        libsi  = glob.glob(op.join(distdir, 'FSLeyes', 'libspatialindex.*'))
        libsic = glob.glob(op.join(distdir, 'FSLeyes', 'libspatialindex_c.*'))
        if len(libsi) != 1 or len(libsic) != 1:
            raise RuntimeError('Cannot identify libspatialindex/_c')

        os.symlink(op.basename(libsi[0]),
                   op.join(distdir, 'FSLeyes', 'libspatialindex.so'))
        os.symlink(op.basename(libsic[0]),
                   op.join(distdir, 'FSLeyes', 'libspatialindex_c.so'))


        # pyinstaller tends to include lots
        # of things that will be provided
        # by the running OS, so we can remove
        # a bunch of cruft.
        libstoremove = ['libasyncns*',
                        'libasound*',
                        'libatk*',
                        'libcaca*',
                        'libcairo*',
                        'libcom_err*',
                        'libcrypto*',
                        'libdatrie*',
                        'libdbus*',
                        'libdrm*',
                        'libEGL*',
                        'libenchant*',
                        'libffi*',
                        'libFLAC*',
                        'libfontconfig*',
                        'libfreetype*',
                        'libgailutil*',
                        'libgbm*',
                        'libgcc_s*',
                        'libgdk*',
                        'libgio*',
                        'libGL.*',
                        'libGLU*',
                        'libglib*',
                        'libglapi*',
                        'libgmodule*',
                        'libgobject*',
                        'libgraphite*',
                        'libgssapi*',
                        'libgst*',
                        'libgthread*',
                        'libgtk*',
                        'libjbig*',
                        'libharfbuzz*',
                        'libICE*',
                        'libicu*',
                        'libk5crypto*',
                        'libkeyutils*',
                        'libkrb*',
                        'libncurses*',
                        'libogg*',
                        'libpango*',
                        'libpcre*',
                        'libpixman*',
                        'libpulse*',
                        'libselinux*',
                        'libslang*',
                        'libSM*',
                        'libsndfile*',
                        'libsoup*',
                        'libssl*',
                        'libstdc*',
                        'libthai*',
                        'libtinfo*',
                        'libuuid*',
                        'libvorbis*',
                        'libwrap*',
                        'libxcb*',
                        'libxml*',
                        'libxslt*',
                        'libX*']

        # libGLU is not present
        # by default on centos7
        if platform == 'centos7':
            libstoremove.remove('libGLU*')

        for ltr in libstoremove:
            paths = glob.glob(op.join(distdir, 'FSLeyes', ltr))
            for p in paths:
                os.remove(p)

        # Directories that can be safely removed
        dirstoremove = ['mpl-data/sample_data']
        for dtr in dirstoremove:
            dtr = op.join(distdir, 'FSLeyes', dtr)
            shutil.rmtree(dtr)

        # Copy assets
        os.makedirs(assetdir)
        for dirname, files in assets:

            dirname = op.join(assetdir, dirname)

            if not op.exists(dirname):
                os.makedirs(dirname)

            for src in files:
                shutil.copy(src, dirname)

        # Make sure permissions are sensible
        for root, dirs, files in os.walk(distdir):
            for path in it.chain(dirs, files):
                os.chmod(os.path.join(root, path) , 0o755)


    def include_package(self, pkgname, ftypes=None):
        """ftypes may be 'all' """

        if ftypes is None:
            ftypes = []

        if ftypes != 'all':
            ftypes.insert(0, '.py')

        pkg     = importlib.import_module(pkgname)
        pkgpath = pkg.__path__[0]

        extrafiles = []

        for dirpath, _, filenames in os.walk(pkgpath):

            if ftypes != 'all':
                filenames = [f for f in filenames
                             if any([f.endswith(ft) for ft in ftypes])]

            dest = op.relpath(dirpath, op.join(pkgpath, '..'))

            for filename in filenames:

                srcfile = op.join(dirpath, filename)
                extrafiles.append('{}:{}'.format(srcfile, dest))

                if srcfile.endswith('.py'):
                    srccmpfile = srcfile + 'c'
                    py_compile.compile(srcfile, srccmpfile)
                    extrafiles.append('{}:{}'.format(srccmpfile, dest))

        return extrafiles


def find_library(name):
    """Returns the path o the given shared library. """

    import ctypes.util as cutil

    path = cutil.find_library(name)

    if path is None:
        raise RuntimeError('Library {} not found'.format(name))

    # Under mac, find_library
    # returns the full path
    if platform == 'darwin':
        return op.realpath(path)

    print('Searching for: {}'.format(path))

    # Under linux, find_library
    # just returns a file name.
    # Let's look for it in some
    # likely locations.
    searchDirs = ['/lib64/',
                  '/lib/',
                  '/usr/lib64/',
                  '/usr/lib/',
                  '/usr/lib/x86_64-linux-gnu',
                  '/usr/local/lib/']
    for sd in searchDirs:
        searchPath = op.join(sd, path)
        if op.exists(searchPath):
            return searchPath

    raise RuntimeError('Could not find location for library {}'.format(name))


def package_path(pkg, in_=False):
    """Returns the directory path to the given python package. """
    fname   = pkgutil.get_loader(pkg).get_filename()
    dirname = op.dirname(fname)

    if in_: return op.abspath(dirname)
    else:   return op.abspath(op.join(dirname, '..'))


def list_all_files(in_dir):
    """List all files ``in_dir``. """

    for dirname, dirs, files in os.walk(in_dir):
        for filename in files:
            yield op.join(dirname, filename)


def build_asset_list(flat):
    """Build and return a list of all the FSLeyes non-source-code files that
    should be included in a distribution. The file paths are made relative
    to the FSLeyes base directory.

    :arg flat: If ``True``, a list is returned. Othewrise, a list of the form::

                   [ (dest_directory, [files_to_put_in_dest_directory]),
                     ...
                   ]

               is returned.
    """

    fsleyesdir = op.join(package_path('fsleyes'), 'fsleyes')
    assetdir   = op.join(fsleyesdir, 'assets')

    # we're either building from a FSLeyes
    # installed into the python environment,
    # or from a git checkout
    from_install = True
    if not op.exists(assetdir):
        from_install = False
        assetdir     = op.abspath(op.join(fsleyesdir, '..', 'assets'))

    if not op.exists(assetdir):
        raise RuntimeError('Cannot find assets/doc dirs! Looked '
                           'in {}'.format(assetdir))

    excludePatterns = [
        op.join(assetdir, 'icons', 'app_icon', '*'),
        op.join(assetdir, 'icons', 'splash', 'sources', '*'),
        op.join(assetdir, 'icons', 'sources', '*'),
        op.join(assetdir, 'build', '*'),
        op.join('*', '.DS_Store'),
    ]

    flist      = defaultdict(list)
    assetfiles = list_all_files(assetdir)

    for filename in assetfiles:

        exclude = any([fnmatch.fnmatch(filename, p) for p in excludePatterns])

        if not exclude:
            destdir = op.relpath(op.dirname(filename), fsleyesdir)
            # need to tweak the path if
            # building from git checkout
            if not from_install:
                destdir = destdir[3:]
            flist[destdir].append(filename)

    if flat:
        return list(it.chain(*[flist[k] for k in flist.keys()]))
    else:
        return list(flist.items())


def install_dcm2niix(destdir):
    """Downloads and installs dcm2niix into ``destdir``. """
    install_script = op.join(basedir, '.ci', 'install_dcm2niix.sh')
    os.system('{} {}'.format(install_script, destdir))


def main():
    setup(
        name='fsleyes-build',
        version='0.0.0',
        description='Standalone builds for FSLeyes',
        url='https://git.fmrib.ox.ac.uk/fsl/fsleyes/build',
        author='Paul McCarthy',
        author_email='pauldmccarthy@gmail.com',
        license='Apache License Version 2.0',
        cmdclass={
            'build_standalone' : build_standalone,
            'py2app'           : py2app,
            'pyinstaller'      : pyinstaller,
        },
    )


if __name__ == '__main__':
    logging.basicConfig()

    def dummy_log(*args, **kwargs):
        pass

    # some things are awfully loud, and
    # distutils does its own logging.
    import distutils.log as dul
    dul._global_log._log = dummy_log

    logging.getLogger('py2app')    .setLevel(logging.CRITICAL)
    logging.getLogger('distutils') .setLevel(logging.CRITICAL)
    logging.getLogger('setuptools').setLevel(logging.CRITICAL)

    main()
