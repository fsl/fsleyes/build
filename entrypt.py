#!/usr/bin/env python
#
# Wrapper around fsleyes.main

import            os
import os.path as op
import            sys
import            platform
import            warnings


# I can't remember why this was necessary, so am just going
# to leave it in. The original comment was:
#
#   >>> temporary hack for 0.24.x: make sure there is a PYTHONPATH
#
os.environ['PYTHONPATH'] = os.environ.get('PYTHONPATH', '')


# make sure dcm2niix is on the PATH
basedir = op.abspath(op.dirname(sys.executable))
if platform.system() == 'Darwin':
    basedir = op.normpath(op.join(basedir, '..', '..'))

dcmdir = op.join(basedir, 'dcm2niix')

if op.exists(dcmdir):
    path               = os.environ['PATH']
    os.environ['PATH'] = os.pathsep.join((dcmdir, path))


# 0.30.0: temporary hack to suppress a warning originating from
# matploitlib:
#
#   The MATPLOTLIBDATA environment variable was deprecated
#   in Matplotlib 3.1 and will be removed in 3.3.
#
# This will be handled in fsleyes.filtermain in the next version
warnings.filterwarnings('ignore', module='fsleyes.profiles')


import fsleyes.filtermain as fm
fm.main()
