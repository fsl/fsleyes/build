``fsleyes-build``
=================


This project contains scripts and CI infrastructure to build standalone
versions of `FSLeyes <https://git.fmrib.ox.ac.uk/fsl/fsleyes/fsleyes>`_ using
`py2app <https://py2app.readthedocs.io/en/latest/>`_ (on macOS) or
`pyinstaller <http://www.pyinstaller.org/>`_ (on Linux).


FSLeyes is built for the following platforms:

 - macOS
 - Ubuntu 14.04
 - Ubuntu 16.04
 - Ubuntu 18.04
 - CentOS 6
 - CentOS 7

Release builds are made available to download from
https://fsl.fmrib.ox.ac.uk/fsldownloads/fsleyes/


In order to build a specific version of FSLeyes, change the ``FSLEYES_REF``
variable in ``.gitlab-ci.yml`` to the git branch/tag you wish to build.

If you need to generate new builds for an existing release, bump the
``FSLEYES_BUILD`` number. But make sure to re-set it to 0 when you build the
next release.
