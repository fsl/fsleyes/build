#!/bin/bash

set -e

##########################################################
# The setup_ssh scrip does the following:
#
#  - Sets up key-based SSH login, and
#    installs the private keys, so
#    we can connect to servers.
#
#  - Configures git
#
# (see https://docs.gitlab.com/ce/ci/ssh_keys/README.html)
#
# NOTE: It is assumed that non-docker
#       executors are already configured
#       (or don't need any configuration).
##########################################################


if [[ -f /.dockerenv ]]; then

  apt-get update -y                           || yum -y check-update                     || true;
  apt-get install -y git rsync openssh-client || yum install -y git rsync openssh-client || true;

  eval $(ssh-agent -s);
  mkdir -p $HOME/.ssh;

  echo "$SSH_PRIVATE_KEY_GIT"          > $HOME/.ssh/id_git;
  echo "$SSH_PRIVATE_KEY_BUILD_DEPLOY" > $HOME/.ssh/id_build_deploy;

  chmod go-rwx $HOME/.ssh/id_*;

  ssh-add $HOME/.ssh/id_git;
  ssh-add $HOME/.ssh/id_build_deploy;

  touch $HOME/.ssh/config;

  BUILD_USER=${BUILD_HOST/@*/}
  BUILD_HOST=${BUILD_HOST/*@/}
  FSLEYES_USER=${FSLEYES_URL/@*/}
  FSLEYES_HOST=${FSLEYES_URL/*@/}
  FSLEYES_HOST=${FSLEYES_HOST/:*/}

  ssh-keyscan ${BUILD_HOST}   >> $HOME/.ssh/known_hosts;
  ssh-keyscan ${FSLEYES_HOST} >> $HOME/.ssh/known_hosts;

  echo "Host ${FSLEYES_HOST}"                          >> $HOME/.ssh/config;
  echo "    User ${FSLEYES_USER}"                      >> $HOME/.ssh/config;
  echo "    IdentityFile $HOME/.ssh/id_git"            >> $HOME/.ssh/config;

  echo "Host builddeploy"                              >> $HOME/.ssh/config;
  echo "    HostName ${BUILD_HOST}"                    >> $HOME/.ssh/config;
  echo "    User ${BUILD_USER}"                        >> $HOME/.ssh/config;
  echo "    IdentityFile $HOME/.ssh/id_build_deploy"   >> $HOME/.ssh/config;

  echo "Host *"                                        >> $HOME/.ssh/config;
  echo "    IdentitiesOnly yes"                        >> $HOME/.ssh/config;

  git config --global user.name  "Gitlab CI";
  git config --global user.email "gitlabci@localhost";
fi
