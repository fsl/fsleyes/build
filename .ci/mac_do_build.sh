#!/bin/bash

set -e

ENV_DIR=`pwd`/fsleyes-build-env/
CONDA_DIR=$(dirname $(which conda))
conda create -y -p $ENV_DIR python=3.6
source $CONDA_DIR/../bin/activate $ENV_DIR

# py2app searches for libpython3.6.dylib, but
# conda environments have libpython3.6m.dylib.
# Something to do with how python gets compiled.
pushd $ENV_DIR/lib/ > /dev/null
ln -s libpython3.6m.dylib libpython3.6.dylib
popd > /dev/null

# Install non-python dependencies
conda install -y -c conda-forge \
      rtree=0.8.3 \
      libspatialindex=1.8.5 \
      libgfortran=3 \
      fortran-compiler \
      clang \
      clangdev \
      pybind11 \
      cython

conda update -y pip setuptools

# py2app is broken, so we use a very specific
# version and manually patch some things
pip install "macholib==1.10"
pip install "py2app==0.14"

# Patch/rebuild py2app (see fsleyes-build/setup.py docstring)
PY2APP=`python -c "import py2app; print(py2app.__file__)"`
BUILDDIR=`pwd`
pushd `dirname $PY2APP`
patch -p2 < $BUILDDIR/resources/py2app.patch
pushd apptemplate
DYLD_LIBRARY_PATH=`pwd` python setup.py
popd
popd

# Install FSLeyes and all of its dependencies
git clone $FSLEYES_URL fsleyes
pushd fsleyes > /dev/null
FSLEYES_DIR=`pwd`
git checkout $FSLEYES_REF

# We must install scipy from source, otherwise
# py2app can't do things to its .dylibs.
# The pre-compiled pyopengl_accelerate
# distribution doesn't include numpy wrappers.
# everything else can be installed as normal
PIPARGS="--retries 10 --timeout 30"

pip install $PIPARGS `cat requirements.txt | grep -i numpy`
export CFLAGS="-Wl,-headerpad_max_install_names"
export F77FLAGS="-Wl,-headerpad_max_install_names"
export F90FLAGS="-Wl,-headerpad_max_install_names"
export FFLAGS="-Wl,-headerpad_max_install_names"
((pip install $PIPARGS --no-binary scipy `cat requirements.txt | grep -i scipy` --log=scipy.log && echo "0" > status) || echo "1" > status) || true
status=`cat status`
if [[ "$status" == "1" ]]; then
  cat scipy.log
  exit 1
fi

pip install $PIPARGS --no-binary pyopengl --no-binary pyopengl-accelerate pyopengl-accelerate `cat requirements.txt | grep -i pyopengl`
pip install $PIPARGS jsonschema==2.6.0
pip install $PIPARGS --ignore-installed certifi
pip install $PIPARGS    dataclasses
pip install $PIPARGS -r requirements.txt
pip install $PIPARGS -r requirements-dev.txt
pip install $PIPARGS -r requirements-extra.txt
pip install $PIPARGS -r requirements-darwin.txt
pip install $PIPARGS -r requirements-notebook.txt
PYTHONPATH=`pwd` pip install -vvv .

# back to fsleyes-build directory
popd > /dev/null

# print env
pip freeze

# Do the build
python setup.py build_standalone

# py2app is getting it all completely wrong,
# so we're going to patch a bunch of things
# and re-generate the tar.gz file.
pushd dist > /dev/null
rm FSLeyes*.tar.gz
popd


# The system libffi on 10.9 is way out of date,
# so we have to include the conda-env copy.
# py2app copies the wrong version of libpng
pushd dist/FSLeyes.app/Contents/Frameworks > /dev/null
cp -a $ENV_DIR/lib/libffi*                                                      .
cp -a $ENV_DIR/lib/libspatialindex*dylib                                        .
cp -a $ENV_DIR/lib/python3.6/site-packages/matplotlib/.dylibs/libpng16.16.dylib .
popd

# various libs are either missed by py2app, or
# we need more up-to-date versions provided
# by conda, than what are available in macos
pushd dist/FSLeyes.app/Contents/Resources/lib > /dev/null
cp -a $ENV_DIR/lib/libssl*dylib    .
cp -a $ENV_DIR/lib/libcrypto*dylib .
cp -a $ENV_DIR/lib/libsqlite*dylib .
ln -s ../../Frameworks/libspatialindex_c.dylib ./libspatialindex_c.dylib
popd > /dev/null


# There are loads of duplicate/
# unnecessary things in the python
# distribution, which take up a
# lot of space
pushd dist/FSLeyes.app/Contents/Resources/lib > /dev/null
find . -name "__pycache__" -exec rm -r "{}" \; || true > /dev/null
find . -name "tests"       -exec rm -r "{}" \; || true > /dev/null
zip -d python36.zip "fsleyes/assets/*"
zip -d python36.zip "sphinx/*"
zip -d python36.zip "sphinx_rtd_theme/*"
zip -d python36.zip "babel/*"
zip -d python36.zip "pytz/*"
zip -d python36.zip "wx/*.dylib"
zip -d python36.zip "PIL/.dylibs/*.dylib"
popd > /dev/null


# scipy libs can't find numpy libs
pushd dist/FSLeyes.app/Contents/Resources/lib/python3.6 > /dev/null
for f in `find scipy -name "*.cpython-36m-darwin.so"`; do
  install_name_tool \
    -add_rpath \
    "@executable_path/../Resources/lib/python3.6/numpy/.dylibs" \
    "$f"
done
popd

# the python .so libs refer to the conda env lib dir.
# this is harmless, but ugly, so i'm cleaning it up
pushd dist/FSLeyes.app/Contents/Resources/lib/python3.6/lib-dynload > /dev/null
for f in *.so; do
  while otool -l "$f" | grep "$ENV_DIR"/lib; do
    install_name_tool -delete_rpath "$ENV_DIR"/lib "$f"
  done
done
popd


pushd dist > /dev/null
echo "Re-creating FSLeyes-"$FSLEYES_REF"-macos.tar.gz"
tar cz --options gzip:compression-level=9 -f FSLeyes-"$FSLEYES_REF"-macos.tar.gz FSLeyes.app
tar cf FSLeyes-"$FSLEYES_REF"-macos.tar.gz FSLeyes.app
popd > /dev/null


# Sanity check - make sure we can start
# FSLeyes and run a simple test. Assuming
# here that we have a display on the mac
# build environment.
conda deactivate

dist/FSLeyes.app/Contents/MacOS/fsleyes -V || true
dist/FSLeyes.app/Contents/MacOS/fsleyes -h || true
# dist/FSLeyes.app/Contents/MacOS/fsleyes render -of file1.png -sz 572 386 -wl 0.001 -19.001 22.999 -hc -hl .ci/3d
# dist/FSLeyes.app/Contents/MacOS/fsleyes render -of file2.png -sz 572 386                          -hc -hl .ci/mesh.vtk

# source $CONDA_DIR/../bin/activate $ENV_DIR
# python .ci/compare_images.py file1.png .ci/build_test_3d_benchmark.png   1000
# python .ci/compare_images.py file2.png .ci/build_test_mesh_benchmark.png 1000

# conda deactivate
rm -r fsleyes-build-env
