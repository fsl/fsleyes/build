#!/bin/bash

set -e

dest=$1

if [ $(uname -s) == "Darwin" ]; then
  suf="mac"
elif [[ `cat /etc/centos-release | grep "release 6."` ]]; then
  suf="lnxCompat"
else
  suf="lnx"
fi

mkdir -p $dest
pushd $dest
fname="dcm2niix_25-Nov-2018_"$suf".zip"

if command -v wget; then
  wget https://github.com/rordenlab/dcm2niix/releases/download/v1.0.20181125/$fname
elif command -v curl; then
  curl -OL https://github.com/rordenlab/dcm2niix/releases/download/v1.0.20181125/$fname
fi

unzip $fname
rm $fname
popd
