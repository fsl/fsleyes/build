#!/bin/bash


if   [[ `cat /etc/centos-release | grep "release 6."` ]]; then
  pushd /spatialindex_build/spatial*
  make uninstall
  find /usr/ -name "*spatialindex*" -delete
  yum remove -y freeglut
  popd
elif [[ `cat /etc/centos-release | grep "release 7."` ]]; then
  pushd /spatialindex_build/spatial*
  make uninstall
  find /usr/ -name "*spatialindex*" -delete
  yum remove -y freeglut
  popd
elif [[ `cat /etc/lsb-release | grep "14.04"` ]]; then
  apt-get remove -y freeglut3 libspatialindex-c3
elif [[ `cat /etc/lsb-release | grep "16.04"` ]]; then
  apt-get remove -y freeglut3 libspatialindex-c4v5
elif [[ `cat /etc/lsb-release | grep "18.04"` ]]; then
  apt-get remove -y freeglut3 libspatialindex-c4v5
fi
