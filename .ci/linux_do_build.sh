#!/bin/bash

set -e

source /build.venv/bin/activate

# temporary - should be in docker
# image def

yum install -y xorg-x11-fonts-Type1 || true
apt install -y locales              || true
locale-gen en_US.UTF-8              || true
locale-gen en_GB.UTF-8              || true
update-locale                       || true


# Install FSLeyes and all of its dependencies
git clone $FSLEYES_URL fsleyes
pushd fsleyes > /dev/null
git checkout $FSLEYES_REF

pip install --upgrade pip setuptools
pip install jsonschema==2.6.0
pip install pyopengl-accelerate
pip install -r requirements-dev.txt
pip install -r requirements.txt
pip install -r requirements-extra.txt
pip install -r requirements-notebook.txt
pip install .

# temp - should be part of fsleyes dependencies
pip install dataclasses

# back to fsleyes-build directory
popd > /dev/null

pip install --upgrade pyinstaller

# print env
pip freeze

# Do the build
python setup.py build_standalone

# Deactivate to make sure the env
# doesn't interfere with execution
deactivate

mv dist/FSLeyes*.tar.gz dist/FSLeyes-"$FSLEYES_REF"-"$OSNAME".tar.gz

# Sanity check - make sure we can
# start FSLeyes, and run a basic
# test. Sleep between calls to xvfb
# otherwise it gets upset. We also
# uninstall some dependencies that
# should be bundled, to make sure
# they are bundled correctly.

tmp=`dirname $0`
pushd $tmp > /dev/null
thisdir=`pwd`
popd > /dev/null
bash $thisdir/uninstall_deps.sh
dist/FSLeyes/fsleyes -V
dist/FSLeyes/fsleyes -h || true

sleep 5
xvfb-run -a -s "-screen 0 640x482x24" dist/FSLeyes/fsleyes -S -r .ci/build_test.py || true

# I think there's a GL driver bug in centos6
if [[ "$OSNAME" != "centos6" ]]; then
  sleep 5
  xvfb-run -a -s "-screen 0 640x480x24" dist/FSLeyes/fsleyes render -of file1.png -sz 572 386 -wl 0.001 -19.001 22.999 -hc -hl .ci/3d || true
  sleep 5
  xvfb-run -a -s "-screen 0 640x480x24" dist/FSLeyes/fsleyes render -of file2.png -sz 572 386                          -hc -hl .ci/mesh.vtk || true
  source /build.venv/bin/activate
  python .ci/compare_images.py file1.png .ci/build_test_3d_benchmark.png   1000 || true
  python .ci/compare_images.py file2.png .ci/build_test_mesh_benchmark.png 1000 || true
fi
